import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../appService';
import { Customer } from '../models/customer';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css'],
  providers: [AppService]
})

export class CustomerFormComponent implements OnInit {
  @Input() id: number ;
  customers: Customer[];
  res: Object[];
  cs: Customer;
  csInfo: FormGroup;
  locations = [
    {id: 0, name: 'Home', selected: false},
    {id: 1, name: 'Apartment', selected: false},
    {id: 2, name: 'Off-site', selected: false}
  ];
  sources =  [
    {id: 0, name: 'Facebook', selected: false},
    {id: 1, name: 'Yelp', selected: false},
    {id: 2, name: 'Google', selected: false},
    {id: 3, name: 'Youtube', selected: false},
    {id: 4, name: 'Instagram', selected: false}
  ];

  constructor( private fb: FormBuilder,
               private appService: AppService,
               private router: Router)  {
                this.getCustomers().subscribe(data => {
                  this.customers = data;
                });
  }

  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  buildSources() {
    const arr = this.sources.map(s => {
      return this.fb.control(s.selected);
    });
    return this.fb.array(arr);
  }
  buildLocations() {
    const arr = this.locations.map(s => {
      return this.fb.control(s.selected);
    });
    return this.fb.array(arr);
  }
  createFormGroup() {
    this.csInfo = this.fb.group({
      firstName:['',
                [Validators.required,
                Validators.maxLength(25),
                Validators.minLength(5)]],
      lastName:['', Validators.required],
      phoneNo:['', [Validators.required, Validators.pattern('[0-9]')]],
      gateCode:'',
      address:'',
      city:'',
      zipCode:'',
      email:['', Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
      location: this.buildLocations(),
      electricity:'',
      water:'',
      vehicle: this.fb.group({
        year:'',
        make:'',
        model:'',
        color:''
      }),
      sources: this.buildSources(),
      note:''
    });
  }

  getCustomers() {
    return this.appService.getCustomers().pipe(map(customers =>
      this.customers = customers
      )
    );
  }
 
  ngOnInit() {
    this.createFormGroup();
      if (this.id === undefined) {
      } else {
        this.appService.getCustomer(this.id.toString()).subscribe(data => {
          this.csInfo.patchValue({
            ...data
          });
        });
      }
    }

  onSubmit() {
    this.cs = new Customer(this.csInfo.value);
    if (this.id === undefined) {
      this.appService.addCustomer(this.cs).subscribe(data => {
        //console.log(data);
      });
    } else {
      this.appService.updateCustomer(this.id.toString(), this.cs).subscribe();
    }
    this.router.navigate(['/dashboard']);
  } 
}
