import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from './models/customer';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
    providedIn: 'root'
})

export class AppService {

    url = 'http://localhost:3000/customers';
    constructor(private http: HttpClient) {
        this.getJSON().subscribe(data => {
        //console.log(data)
    });
    }

    public getJSON(): Observable<any> {
        return this.http.get(this.url,  {responseType: 'text'});
    }

    public addCustomer (customer: Customer): Observable<Customer> { 
        return this.http.post<Customer>(this.url , customer, httpOptions);
    }

    public updateCustomer (id: string, customer: Customer):  Observable<Customer> {
        return this.http.put<Customer>(this.url + '/' + id, customer, httpOptions);
    }

    public getCustomers(): Observable<Customer[]>{
        return this.http.get<Customer[]>(this.url);
    }

    public getCustomer(id: string): Observable<Customer> {
        return this.http.get<Customer>(this.url + '/' + id);
    }
}