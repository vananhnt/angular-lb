export class Vehicle {
    year: number;
    make: string;
    model: string;
    color: string;
}