import { Vehicle } from 'src/app/models/vehicle';

export class Customer {
    id: number;
    firstName: string;
    lastName: string;
    phoneNo: string;
    gateCode: number;
    address: string;
    city: string;
    zipCode: number;
    email: string;
    location: string;
    electricity: number;
    water: number;
    vehicle: Vehicle;
    //sources
    note: string;

    public constructor(init?: Partial<Customer >) {
        Object.assign(this, init);
    }
}