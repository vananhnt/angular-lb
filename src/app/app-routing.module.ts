import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerFormBoardComponent } from './customer-form-board/customer-form-board.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'create', component: CustomerFormBoardComponent },
  { path: 'update/:id', component: CustomerFormBoardComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
