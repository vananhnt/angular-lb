import { Component, OnInit, Input } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Customer } from '../models/customer';
import { AppService } from '../appService';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.css'],
  providers: [AppService],
  template: `
    <ng2-smart-table [settings]="settings" [source]="data" (userRowSelect)="onUserRowSelect($event)"></ng2-smart-table>
  `
})

export class CustomerTableComponent implements OnInit {
  customers: Customer[];
  settings = {
    actions: {
      add: false,
      edit: false,
      // custom: [{
      //   name: 'edit',
      //   title: '<button type="button" class="btn btn-primary" (click)="onClick($event)" >Edit</button>'
      // }],
      position: 'right'
    },
    columns: {
      id: {
        title: 'ID'
      },
      firstName: {
        title: 'First name'
      },
      lastName: {
        title: 'Last name'
      },
      email: {
        title: 'Email'
      },
      phoneNo: {
        title: 'Phone'
      }
    },
    attr: {
        class: 'table table-bordered'
      }
  };
  data: Object[];
  constructor(private appService: AppService,
    private router: Router) {
    this.appService.getCustomers().subscribe(res => {
      this.customers = res;
       this.data = this.customers.map(element => ({
         id: element.id,
         firstName: element.firstName,
         lastName: element.lastName,
         email: element.email,
         phoneNo: element.phoneNo
       }));
      });
  }

  onUserRowSelect(event): void {
    this.router.navigate(['/update/', event.data.id]);
  }
  ngOnInit() {
  }

}
