import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customer-form-board',
  templateUrl: './customer-form-board.component.html',
  styleUrls: ['./customer-form-board.component.css']
})
export class CustomerFormBoardComponent implements OnInit {
  pid: number;
  private sub: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.pid = +params['id']; // (+) converts string 'id' to a number

   });
  }

}
