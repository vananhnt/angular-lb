import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFormBoardComponent } from './customer-form-board.component';

describe('CustomerFormBoardComponent', () => {
  let component: CustomerFormBoardComponent;
  let fixture: ComponentFixture<CustomerFormBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFormBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFormBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
